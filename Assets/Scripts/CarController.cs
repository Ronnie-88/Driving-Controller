﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    private CarInput _CarInput;
    [SerializeField]
    private float _DriveSpeed;
    [SerializeField]
    private float _BreakSpeed;
    [SerializeField]
    private float _MaxSteeringAngle;
    [SerializeField]
    private WheelCollider _FrontLeftWC;
    [SerializeField]
    private WheelCollider _FrontRightWC;
    [SerializeField]
    private WheelCollider _RearLeftWC;
    [SerializeField]
    private WheelCollider _RearRightWC;
    [SerializeField]
    private Transform _FrontLeftWT;
    [SerializeField]
    private Transform _FrontRightWT;
    [SerializeField]
    private Transform _RearLeftWT;
    [SerializeField]
    private Transform _RearRightWT;
    private float _CurrentBreakForce;
    private float _SteeringAngle;
    float torqueVal;

    private void Awake()
    {
        _CarInput = GetComponent<CarInput>();
    }

    private void FixedUpdate()
    {
        DriveMotor();
        ApplySteering();
        UpdateWheelVisuals();
    }

    private void DriveMotor()
    {
        _FrontLeftWC.motorTorque = _CarInput.ThrottoleAxis * _DriveSpeed;
        _FrontRightWC.motorTorque = _CarInput.ThrottoleAxis * _DriveSpeed;
        _CurrentBreakForce = (_CarInput.ThrottoleAxis <= 0.0f) ? _BreakSpeed : 0.0f;

        if (_CarInput.ThrottoleAxis <= 0.0f)
        {
            ApplyBreaking();
        }

    }

    private void ApplyBreaking()
    {
        torqueVal = _CarInput.ThrottoleAxis * _CurrentBreakForce;
        _FrontLeftWC.brakeTorque = _CarInput.ThrottoleAxis * -_CurrentBreakForce;
        _FrontRightWC.brakeTorque = _CarInput.ThrottoleAxis * -_CurrentBreakForce;
        _RearLeftWC.brakeTorque = _CarInput.ThrottoleAxis * -_CurrentBreakForce;
        _RearRightWC.brakeTorque = _CarInput.ThrottoleAxis * -_CurrentBreakForce;
    }

    private void ApplySteering()
    {
        _SteeringAngle = _MaxSteeringAngle * _CarInput.SteerAxis;
        _FrontLeftWC.steerAngle = _SteeringAngle;
        _FrontRightWC.steerAngle = _SteeringAngle;
    }

    private void UpdateWheelVisuals()
    {
        UpdateSingleWheel(_FrontLeftWC, _FrontLeftWT);
        UpdateSingleWheel(_FrontRightWC, _FrontRightWT);
        UpdateSingleWheel(_RearLeftWC, _RearLeftWT);
        UpdateSingleWheel(_RearRightWC, _RearRightWT);
    }

    private void UpdateSingleWheel(WheelCollider wC, Transform wT)
    {
        Vector3 wPos;
        Quaternion wRot;

        wC.GetWorldPose(out wPos, out wRot);
        wT.position = wPos;
        wT.rotation = wRot;
    }
}
