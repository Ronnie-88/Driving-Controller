﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CarInput : MonoBehaviour
{
    private DriveContollerInput _Input;
    public float ThrottoleAxis { get; private set; }
    public float SteerAxis { get; private set; }

    private void Awake()
    {
        _Input = new DriveContollerInput();
    }

    private void OnEnable()
    {
        SubFunctions();
    }

    private void OnDisable()
    {
        UnSubFunctions();    
    }

    private void SubFunctions()
    {
        _Input.GamePlay.Throttle.performed += OnThrottlePerformed;
        _Input.GamePlay.Throttle.canceled += OnThrottleCanceled;
        _Input.GamePlay.Steer.performed += OnSteeringPerformed;
        _Input.GamePlay.Steer.canceled += OnSteeringCanceled;
        _Input.GamePlay.Enable();
    }

    private void UnSubFunctions()
    {
        _Input.GamePlay.Throttle.performed -= OnThrottlePerformed;
        _Input.GamePlay.Throttle.canceled -= OnThrottleCanceled;
        _Input.GamePlay.Steer.performed -= OnSteeringPerformed;
        _Input.GamePlay.Steer.canceled -= OnSteeringCanceled;
        _Input.GamePlay.Disable();
    }

    private void OnThrottlePerformed(InputAction.CallbackContext context)
    {
        ThrottoleAxis = context.ReadValue<float>();
    }

    private void OnThrottleCanceled(InputAction.CallbackContext context)
    {
        ThrottoleAxis = 0.0f;
    }
    private void OnSteeringPerformed(InputAction.CallbackContext context)
    {
        SteerAxis = context.ReadValue<float>();
    }

    private void OnSteeringCanceled(InputAction.CallbackContext context)
    {
        SteerAxis = 0.0f;
    }



}
